const { response } = require('express');
let express = require('express');
let app = express();
const port = 8000;


//recurso principal
app.get('/',(request, response)=>{
    response.send("{message: método get principal}");
})// sempre tem que ter o caminho '/', request e response serve para sabermos oq foi solicitado e oq vamos retornar


//recurso funcionario
app.get('/funcionario',(request, response)=>{
    let obj = request.query;
    let nome = obj.nome;
    let sobrenome = obj.Sobrenome;
    response.send("{message: método get funcionario" + nome + "}");
})


//habilitando o servico na porta 8000


app.listen(port,function(){
    console.log("Projeto rodando na porta:"+ port);
});